#!/usr/bin/env python3

from io import BytesIO
from lxml import html

import configparser
import datetime
import pprint
import pycurl
import requests
import string
import time

global_job_dictionary = {}
global_session_key = ""
global_session_value = ""

global_stat_daily_new_jobs = 0
global_stat_email_sent_count = 0
global_stat_error_count = 0
global_stat_false_count = 0
global_stat_warning_count = 0
global_stat_session_count = 0
global_stat_check_count = 0

def send_message(new_number, message, html_message, config):
    global global_stat_email_sent_count
    global global_stat_daily_new_jobs

    try:
        requests.post(
            config['mail']['link'],
            auth=("api", config['mail']['auth']),
            data={"from": config['mail']['data_from'],
                  "to": config['mail']['data_to'].split(","),
                  "subject": "{} {}".format(config['mail']['data_subject'], new_number),
                  "text": message,
                  "html": html_message})

        global_stat_email_sent_count += 1
        global_stat_daily_new_jobs += new_number
    except Exception:
        log("Failed to send email api request", "ERROR")

def send_daily_statistics(config):
    global global_stat_email_sent_count
    global global_stat_daily_new_jobs
    global global_stat_session_count
    global global_stat_check_count
    global global_stat_error_count
    global global_stat_false_count
    global global_stat_warning_count

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

    message  = '<p style="font-family: "Courier New", Courier, monospace">\n'
    message += "New daily jobs: {}<br>\n".format(global_stat_daily_new_jobs)
    message += "Sent emails:    {}<br>\n".format(global_stat_email_sent_count)
    message += "Session count:  {}<br>\n".format(global_stat_session_count)
    message += "Check count:    {}<br>\n".format(global_stat_check_count)
    message += "Error count:    {}<br>\n".format(global_stat_error_count)
    message += "False count:    {}<br>\n".format(global_stat_false_count)
    message += "Warning count:  {}<br>\n".format(global_stat_warning_count)
    message += "</p>\n"

    try:
        requests.post(
            config['mail']['link'],
            auth=("api", config['mail']['auth']),
            data={"from": config['mail']['data_from'],
                  "to": config['mail']['data_to_statistics'].split(","),
                  "subject": "{} {}".format(config['mail']['data_subject_statistics'], timestamp),
                  "text": message,
                  "html": message})

        # reset counters
        global_stat_email_sent_count = 0
        global_stat_daily_new_jobs = 0
        global_stat_session_count = 0
        global_stat_check_count = 0
        global_stat_error_count = 0
        global_stat_false_count = 0
        global_stat_warning_count = 0
    except Exception:
        log("Failed to send email api request", "ERROR")

def get_km_html(page):
    global global_session_key
    global global_session_value
    global global_stat_check_count

    cookies_string = 'Cookie: SOOrderedParts=Counter=; SOSurveyOH=Counter=; SOOrderjournal=Counter=; SOSurvey=ResGroupFilter=TECHNICIANS&Filter=w/o%20IDR&SkillFilter=ALL&SortType=ALL&RespTime=ALL&Counter=0; {}={};'.format(global_session_key, global_session_value)

    http_header = [
        'Accept-Encoding: gzip, deflate, br',
        'Accept-Language: en-US,en;q=0.9,sk-SK;q=0.8,sk;q=0.7',
        'Upgrade-Insecure-Requests: 1',
        'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Cache-Control: max-age=0',
        'Cookie: SOOrderedParts=Counter=; SOSurveyOH=Counter=; SOOrderjournal=Counter=; SOSurvey=ResGroupFilter=TECHNICIANS&Filter=w/o%20IDR&SkillFilter=ALL&SortType=ALL&RespTime=ALL&Counter=0; {}={};'.format(global_session_key, global_session_value),
        'Connection: keep-alive'
    ]

    buffer = BytesIO()
    c = pycurl.Curl()

    c.setopt(pycurl.URL, config['km']['url'].format(page))
    c.setopt(pycurl.HTTPHEADER, http_header)
    c.setopt(pycurl.MAXREDIRS, 5)
    c.setopt(pycurl.TCP_KEEPALIVE, 1)
    c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_LAST)
    c.setopt(pycurl.NOPROGRESS, 1)
    c.setopt(pycurl.FOLLOWLOCATION, 1)
    c.setopt(pycurl.SSL_VERIFYHOST, 0)
    c.setopt(pycurl.WRITEDATA, buffer)

    try:
        c.perform()
        c.close()
        global_stat_check_count += 1
    except:
        log("Failed to curl page", "ERROR")
        return bytes()

    return buffer.getvalue()

def get_page_dictionary(html_content):
    global global_stat_false_count

    single_page_job_dictionary = {}
    try:
        tree = html.fromstring(html_content)
    except Exception:
        log("Failed to get xml tree from html: {}".format(html_content))
        global_stat_false_count += 1
        return False

    header = ["_1", "_2", "_3", "st", "rc", "d_type_due", "name", "city_street", "si_con_exp", "so", "mb_text", "operating_time"]
    job_id_list = tree.xpath('//*[@id="content_wrap_table"]//input[@name="sno"]/@value')

    for job_id in job_id_list:
        job_dict = {}

        for td_index in range(12):
            cell_array = tree.xpath('//*[@id="content_wrap_table"]//input[@value="{}"]/parent::*/parent::tr//td[{}]//text()'.format(job_id, td_index + 1))
            try:
                # convert cell_array elements to string and join with ' '
                array_string = ' '.join(map(str, cell_array))
                # replace newline and carriage return
                array_string = array_string.replace('\n', '').replace('\r', '')
                # split and join to remove multiple spaces
                array_string = ' '.join(array_string.split())
                # insert into dictionary as value with correct key
                job_dict[header[td_index]] = array_string
            except Exception:
                log("Failed to parse td_index = {}. Cell array: {}".format(td_index, cell_array), "WARNING")

        if not single_page_job_dictionary.__contains__(job_id):
            single_page_job_dictionary[job_id] = job_dict
        else:
            log("Duplicate dictionary key: {}".format(job_id))

    return single_page_job_dictionary

def get_page_count(html_content):
    if len(html_content) == 0:
        return 0

    tree = html.fromstring(html_content)

    pc_string = tree.xpath('//*[@id="table_main"]//tr[3]//td[1]//td[2]//font/text()')
    try:
        pc_int = int(pc_string[0].replace('\r', '').replace('\n', '').replace(')', '').replace(' ', '').split('/')[1])
    except Exception:
        log("Unable to index into page tree. Html: {}".format(html_content), "ERROR")
        return 0

    return pc_int

def get_new_session(config):
    global global_session_key
    global global_session_value
    global global_stat_session_count

    payload = {
        "au": config['km']['au'],
        "ap": config['km']['ap']
    }

    log("Getting new session")
    session_requests = requests.session()

    try:
        session_requests.post(config['km']['url_login'], data = payload)
        cookies = session_requests.cookies.get_dict()

        response = session_requests.get(config['km']['url'])
        cookies = session_requests.cookies.get_dict()

        global_stat_session_count += 1
    except Exception:
        log("Failed to retrieve new session", "WARNING")
        return

    for key in cookies.keys():
        if "ASPSESSIONID" in key:
            global_session_key = key
            global_session_value = cookies.get(key)
            log("Session cookie set: {}={}".format(key, cookies.get(key)))

def get_new_jobs(config):
    global global_job_dictionary
    first_page = get_km_html(0)

    page_count = get_page_count(first_page)

    if page_count == 0:
        get_new_session(config)

    new_global_job_dictionary = {}

    # iterate other pages and generate new jobs array
    for page_index in range(0, page_count):
        if page_index == 0:
            page_dictionary = get_page_dictionary(first_page)
        else:
            page_dictionary = get_page_dictionary(get_km_html(page_index))

        if page_dictionary != False:
            for k in page_dictionary.keys():
                if not new_global_job_dictionary.__contains__(k):
                    new_global_job_dictionary[k] = page_dictionary[k]
                else:
                    # should not happen as this is checked previosly
                    log("Duplicate global key: {} => {} | {}".format(k, page_dictionary[k], new_global_job_dictionary[k]), "ERROR")

    new_job_listings = {}
    if len(new_global_job_dictionary.keys()) > 0:
        # compare old global with new global dictionary
        for k in new_global_job_dictionary.keys():
            if not global_job_dictionary.__contains__(k):
                new_job_listings[k] = new_global_job_dictionary[k]

        global_job_dictionary = new_global_job_dictionary

    return new_job_listings

def format_data(job_array):
    header = ["st", "rc", "d_type_due", "name", "city_street", "si_con_exp", "so", "mb_text", "operating_time"]

    titles = {
        "_1": "_1",
        "_2": "_2",
        "_3": "_3",
        "st": "St.",
        "rc": "Rc",
        "d_type_due": "D.Type/Due",
        "name": "Name",
        "city_street": "City/Street",
        "si_con_exp": "SI/Con.Exp",
        "so": "SO",
        "mb_text": "MB Text 1",
        "operating_time": "Operating Time"
    }

    body = "<html>"

    for k in job_array.keys():
        body += "<table style=\"border-collapse: collapse; border: 1px solid black;\">"
        for tag in header:
            body += "<tr><td style=\"border: 1px solid black;\">{}</td><td style=\"border: 1px solid black;\">{}</td></tr>".format(titles[tag], job_array[k][tag])
        body += "</table><br><br>"

    body += "</html>"
    return body

def log(message, level = "INFO"):
    global global_stat_error_count
    global global_stat_warning_count

    if level == "ERROR":
        global_stat_error_count += 1
    elif level == "WARNING":
        global_stat_warning_count += 1

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    with open("./km_mail.log", "a") as log_file:
        try:
            log_file.write("[{}][{}] {}\n".format(timestamp, level, message))
            log_file.flush()
        except:
            print("[{}][{}] {}\n".format(timestamp, level, message))

## SCRIPT ##

log("Starting new KM Mail process")
send_stat = True
config = configparser.ConfigParser()
try:
    config.read('./config.ini')
except:
    print("Failed to read config file. Exiting!")
    exit()

long_timeout = int(config['config']['timeout_long'])
normal_timeout = int(config['config']['timeout_normal'])

while True:
    job_array = get_new_jobs(config)
    new_job_count = len(job_array)

    if (new_job_count > 0):
        body = format_data(job_array)
        send_message(new_job_count, body, body, config)
    else:
        pass

    # limit during weekend and night (sleep 1h)
    now = datetime.datetime.now()
    week_day = datetime.datetime.today().weekday()
    evening_limit = now.replace(hour=22, minute=0, second=0, microsecond=0)
    morning_limit = now.replace(hour=6, minute=0, second=0, microsecond=0)

    if now >= evening_limit or now <= morning_limit:
        if send_stat == True:
            send_daily_statistics(config)
            send_stat = False

        time.sleep(long_timeout)
    else:
        send_stat = True

        if week_day == 5 or week_day == 6:
            time.sleep(long_timeout)
        else:
            time.sleep(normal_timeout)
